package com.example.itunesartistsearch.ui.main

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.itunesartistsearch.databinding.MainFragmentBinding
import com.example.itunesartistsearch.ui.adapters.ArtistSearchResultsAdapter
import com.example.itunesartistsearch.utils.hideKeyboard

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: ArtistSearchResultsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.artistSearchResults.observe(this,
            { results ->
                adapter = ArtistSearchResultsAdapter(results)
                binding.searchResultsRecyclerview.layoutManager = LinearLayoutManager(activity)
                binding.searchResultsRecyclerview.adapter = adapter
            })
        viewModel.isLoading.observe(this,
            { isLoading ->
                binding.loadingSpinner.visibility = if(isLoading) View.VISIBLE else View.GONE
            })
        binding.searchButton.setOnClickListener {
            viewModel.searchForArtist()
            hideKeyboard()
        }
        binding.searchInputEditText.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    viewModel.searchForArtist()
                    hideKeyboard()
                    return true
                }
                return false
            }
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this
    }
}