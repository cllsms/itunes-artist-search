package com.example.itunesartistsearch.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.itunesartistsearch.data_models.ArtistResultItem
import com.example.itunesartistsearch.data_models.ResultsResponse
import com.example.itunesartistsearch.repositories.ArtistSearchResultsRepo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel: ViewModel() {
    private val repo = ArtistSearchResultsRepo()
    var searchText: MutableLiveData<String> = MutableLiveData("")
    var artistSearchResults: MutableLiveData<List<ArtistResultItem>> = MutableLiveData()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData(false)

    fun searchForArtist() {
        isLoading.postValue(true)
        searchText.value?.let {
            repo.getSearchResultsForText(it, object : Callback<ResultsResponse> {
                override fun onResponse(
                    call: Call<ResultsResponse>,
                    response: Response<ResultsResponse>
                ) {
                    if (response.body() != null) {
                        artistSearchResults.postValue(response.body()?.results)
                        isLoading.postValue(false)
                    }
                }

                override fun onFailure(call: Call<ResultsResponse>, t: Throwable) {
                    isLoading.postValue(false)
                }
            })
        }
    }
}