package com.example.itunesartistsearch.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.itunesartistsearch.data_models.ArtistResultItem
import com.example.itunesartistsearch.databinding.SearchResultItemLayoutBinding

class ArtistSearchResultsAdapter(private val mValues: List<ArtistResultItem>) : RecyclerView.Adapter<ArtistSearchResultsAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: SearchResultItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ArtistResultItem) {
            binding.resultItem = item
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SearchResultItemLayoutBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mValues[position])
        with(holder.binding.root) {
            tag = mValues[position]
        }
    }

    override fun getItemCount(): Int = mValues.size
}