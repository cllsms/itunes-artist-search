package com.example.itunesartistsearch.repositories

import com.example.itunesartistsearch.data_models.ResultsResponse
import com.example.itunesartistsearch.network.ArtistSearchService
import retrofit2.Callback

class ArtistSearchResultsRepo() {
    fun getSearchResultsForText(enteredText: String, resultsCallback: Callback<ResultsResponse>) {
        val artistSearchAPI = ArtistSearchService.create()
        return artistSearchAPI.getCatalogListingsForArtist(enteredText).enqueue(resultsCallback)
    }
}