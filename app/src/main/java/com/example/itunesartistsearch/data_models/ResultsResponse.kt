package com.example.itunesartistsearch.data_models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResultsResponse(
    @SerializedName("resultsCount")
    @Expose
    val resultsCount: Int?,

    @SerializedName("results")
    @Expose
    val results: ArrayList<ArtistResultItem>?
)