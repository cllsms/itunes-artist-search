package com.example.itunesartistsearch.data_models

import android.os.Parcel
import android.os.Parcelable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.google.gson.annotations.SerializedName
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.util.*

data class ArtistResultItem(

    @SerializedName("artistName")
    val artistName: String?,

    @SerializedName("trackName")
    val trackName: String?,

    @SerializedName("releaseDate")
    val releaseDate: String?,

    @SerializedName("primaryGenreName")
    val primaryGenreName: String?,

    @SerializedName("trackPrice")
    val trackPrice: String?,

    @SerializedName("artworkUrl100")
    val artworkUrl100: String?,

    @SerializedName("collectionName")
    val collectionName: String?,

    @SerializedName("collectionPrice")
    val collectionPrice: String?,

    @SerializedName("wrapperType")
    val wrapperType: String?

): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(artistName)
        dest?.writeString(trackName)
        dest?.writeString(releaseDate)
        dest?.writeString(primaryGenreName)
        dest?.writeString(trackPrice)
        dest?.writeString(artworkUrl100)
        dest?.writeString(collectionName)
        dest?.writeString(collectionPrice)
        dest?.writeString(wrapperType)
    }

    companion object CREATOR : Parcelable.Creator<ArtistResultItem> {
        override fun createFromParcel(parcel: Parcel): ArtistResultItem {
            return ArtistResultItem(parcel)
        }

        override fun newArray(size: Int): Array<ArtistResultItem?> {
            return arrayOfNulls(size)
        }
    }

    fun getName(): String? {
        return if (wrapperType.equals("track")) trackName else collectionName
    }

    fun getFormattedPrice(): String {
        val price = if (wrapperType.equals("track")) trackPrice else collectionPrice

        if (price.isNullOrEmpty()) return ""
        return NumberFormat.getCurrencyInstance(Locale.US).format(price.toDouble())
    }
}

@BindingAdapter(value = ["setImageUrl"])
fun ImageView.bindImageUrl(url: String?) {
    if (url != null && url.isNotBlank()) {
        Picasso.get()
            .load(url)
            .into(this)
    }
}