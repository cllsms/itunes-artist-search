package com.example.itunesartistsearch.network

interface ResponseListener<T> {
    /**
     * Handles a success response from the server
     *
     * @param response [java.lang.Object]
     */
    fun onCompletionSuccess(response: T)

    /**
     * Handles a failure response from the server
     *
     * @param failureMessage [java.lang.String]
     */
    fun onCompletionFailure(failureMessage: String?)
}