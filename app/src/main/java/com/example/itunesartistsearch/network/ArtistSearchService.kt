package com.example.itunesartistsearch.network

import com.example.itunesartistsearch.data_models.ResultsResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.Query

interface ArtistSearchService {
    @GET("search")
    fun getCatalogListingsForArtist(@Query("term") searchText: String): Call<ResultsResponse>

    companion object {
        fun create(): ArtistSearchService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl("https://itunes.apple.com/")
                .build()

            return retrofit.create(ArtistSearchService::class.java)
        }
    }
}